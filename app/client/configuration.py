
from app.utils.singleton import Singleton
import os

class Configuration(metaclass=Singleton):
    def __init__(self):
        self.api_url = os.environ["API_URL"]
        self.output_dir = os.environ["FILE_OUTPUT_DIR"]

    def getApiUrl(self):
        return self.api_url
    
    def getOutputDir(self):
        return self.output_dir