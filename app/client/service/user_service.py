
from typing import List
from app.business_object.user.user import User
from app.client.exception.user_not_authenticated_exception import UserNotAuthenticated
from app.client.web.user_client import UserClient
from app.utils.password_utils import PasswordUtils



class UserService:

    @staticmethod
    def createUser(user: User) -> User:
        return UserClient.createUser(user)

    @staticmethod
    def authenticate_and_get_user(username: str, password: str) -> User:
        userAsDict = UserClient.authenticate_and_get_user(username,password)
        hashedPassword = PasswordUtils.hashPassword(password)
        if userAsDict['username'] == username :
            if userAsDict['password'] == hashedPassword : 
                return User(username,password)
        return None

    @staticmethod
    def update_user(user_name: str, user: User) -> User:
        return UserClient.updateUser(user_name, user)

    @staticmethod
    def delete_user(user_name: str) -> User:
        return UserClient.deleteUser(user_name)

    @staticmethod
    def get_all_users():
        return UserClient.getAllUsers()