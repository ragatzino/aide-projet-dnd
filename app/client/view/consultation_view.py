from pprint import pprint
from PyInquirer import  prompt
from app.client.service.user_service import UserService

from app.client.view.abstract_view import AbstractView
from app.client.view.session import Session


class ConsultationView(AbstractView):
    def __init__(self):
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': '---',
                'choices': [
                    'Retour au menu principal'
                ]
            }
        ]

    def display_info(self):
        print("Liste des utilisateurs")
        

    def make_choice(self):
        userList = UserService().get_all_users()
        for user in userList:
            pprint(user.username)
        answers = prompt(self.__questions)
        if answers['choix'] == "Retour au menu principal":
            from view.start_view import StartView
            return StartView()
