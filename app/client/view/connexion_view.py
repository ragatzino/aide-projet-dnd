from pprint import pprint
from PyInquirer import  prompt
from app.client.service.user_service import UserService

from app.client.view.abstract_view import AbstractView
from app.client.view.session import Session


class ConnexionView(AbstractView):
    def __init__(self) -> None:
        self.__questions = [
            {
                'type': 'input',
                'name': 'pseudo',
                'message': 'What\'s your pseudo',
            },
            {
                'type': 'password',
                'name': 'password',
                'message': 'What\'s your password.',
            }
        ]

    def display_info(self):
        print("Veuillez vous connecter.")

    def make_choice(self):
        answers = prompt(self.__questions)
        pseudo = answers['pseudo']
        password = answers['password']
        foundUser = UserService.authenticate_and_get_user(pseudo,password)
        session = Session()
        if (foundUser != None):
            session.user_name = foundUser.username
            session.user_mdp = foundUser.password
        from view.start_view import StartView
        return StartView()
