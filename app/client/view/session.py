from app.utils.singleton import Singleton


class Session(metaclass=Singleton):
    def __init__(self):
        """
        Définition des variables que l'on stocke en session
        Le syntaxe
        ref:type = valeur
        permet de donner le type des variables. Utile pour l'autocompletion.
        """
        self.user_name: str = "Bob"
        self.user_mdp: str = None
    
    def updateUserName(self,new_username:str):
        self.user_name=new_username
    
    def updateUserName(self,user_mdp:str):
        self.user_mdp=user_mdp