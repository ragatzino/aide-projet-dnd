
from app.utils.singleton import Singleton
import os

class WebConfig(metaclass=Singleton):
    def __init__(self):
        self.api_port = os.environ["API_PORT"]

    def getApiPort(self) -> str:
        return str(self.api_port)
    