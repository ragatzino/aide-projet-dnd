from app.business_object.user.user import User
from app.web_service.dao.configuration import DBConnection
from app.web_service.exception.user_not_found_exception import UserNotFoundException


class UserDao:

    @staticmethod
    def verifyPassword(username: str, password: str) -> bool:
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    str.format(
                    "SELECT * "
                    "\nFROM public.user where username='{}' and password='{}'",username,password
                    )
                )
                res = cursor.fetchone()
            if res["username"] != None:
                return True
            return False

    @staticmethod
    def getUser(username: str) -> User:
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    str.format(
                    "SELECT * "
                    "\nFROM public.user where username='{}'",username
                    )
                )
                res = cursor.fetchone()
        if res:
            return createUserFromRow(res)
        else:
            raise UserNotFoundException(username)

    @staticmethod
    def createUser(user: User) -> User:
        try:
            UserDao.getUser(user.username)
        except UserNotFoundException:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                       str.format("INSERT INTO public.user (id,username, password) VALUES "
                        "(DEFAULT, '{}','{}');",user.username,user.password))
            return UserDao.getUser(user.username)

    @staticmethod
    def updateUser(username: str, user: User) -> User:
        user_to_update: User = UserDao.getUser(username)
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    str.format("UPDATE public.user SET username='{}', password='{}' where id={};", user.username, user.password,user_to_update.id)
                    )

    @staticmethod
    def deleteUser(username: str) -> User:
        user_to_delete: User = UserDao.getUser(username)
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    str.format("Delete from public.user where id={};", user_to_delete.id)
                    )
    @staticmethod
    def get_all_users():
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "select * from public.user")
                res = cursor.fetchall()
            if len(res)>0:
                users = []
                for row in res:
                    rowAsUser = createUserFromRow(row)
                    users.append(rowAsUser)
                return users
            else:
                raise UserNotFoundException("Aucun utilisateur n'a été trouvé")


def createUserFromRow(row):
    return User(id=row["id"], username=row["username"], password=row["password"])