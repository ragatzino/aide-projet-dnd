import dotenv
from fastapi import FastAPI
import uvicorn

from app.web_service.controller import auth_router
from app.web_service.web_configuration import WebConfig

dotenv.load_dotenv(override=True)


app = FastAPI()

app.include_router(auth_router.router)

if __name__ == "__main__":
    port = WebConfig().getApiPort()
    uvicorn.run("webservice:app", host="localhost", port=port, log_level="info")