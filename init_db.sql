DROP TABLE IF EXISTS user CASCADE;

CREATE TABLE user (
    id serial PRIMARY KEY,
    username text UNIQUE NOT NULL,
    password text NOT NULL
);